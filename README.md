# Frontend Mentor - REST Countries API with color theme switcher solution

---

This is my solution to the [REST Countries API with color theme switcher challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/rest-countries-api-with-color-theme-switcher-5cacc469fec04111f7b848ca). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## The challenge

---

Users should be able to:

- See all countries from the API on the homepage
- Search for a country using an `input` field
- Filter countries by region
- Click on a country to see more detailed information on a separate page
- Click through to the border countries on the detail page
- Toggle the color scheme between light and dark mode _(optional)_

## Built with

---

- [React](https://reactjs.org/)
- [Create React App](https://create-react-app.dev/)
- CSS modules
- [Radium](https://formidable.com/open-source/radium/) - for styling certain element states on theme switch
- [React Select](https://react-select.com/home) - for Select Input control
- [Axios](https://github.com/axios/axios) - for making HTTP requests
- [React Router](https://reactrouter.com/) - routing library
- [REST Countries API](https://restcountries.eu/)
- [Font Awesome](https://fontawesome.com/) - Icons
