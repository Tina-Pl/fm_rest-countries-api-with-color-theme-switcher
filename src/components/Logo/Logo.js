import React from 'react';
import classes from './Logo.module.css';

const Logo = () => <div className={classes.Logo}>Where in the world?</div>;

export default Logo;
